<?php require 'php/resultats.php'; ?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/resultats.css">
    <script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
    <title>Trombinouc- Résultat(s) de "<?= $_GET['search'] ?>"</title>
</head>
<body>
    
    <div id="mainLayout">

        <div id="topBar">
            <h1 id="title"><span class="fas fa-globe"></span> Trombinouc <span class="fas fa-globe"></span></h1>
            <div id="profil"><a href="profil.php">Mon profil <span class="fas fa-user"></span></a></div>
            <p id="text">Résultat(s) de "<?php echo $_GET['search'] ?>"</p>
        </div>
    
        <div id="result">
            <?php

                $search = $bdd->prepare("SELECT user_id, prenom, nom, trombitag, love FROM users 
                WHERE (prenom LIKE '%".trim($_GET['search'])."%' 
                OR nom LIKE '%".trim($_GET['search'])."%' 
                OR trombitag LIKE '%".trim($_GET['search'])."%') 
                AND user_id != ".$infoUser['user_id']."");
                $search->execute();
                if($search->rowCount() > 0){
                    foreach($search as $display){
                        if($display['love'] == 1){
                            $love = "<span class='red fas fa-heart'></span>";
                        }else{
                            $love = "<span class='red fas fa-heart-broken'></span>";
                        }
                        echo "<div class='line'>
                            <a href='user.php?tag=".$display['trombitag']."'>
                                ".$love." ".$display['prenom']." ".$display['nom']."
                                <br>
                                ".$display['trombitag']."#".$display['user_id']."
                            </a>
                        </div>";
                    }
                }else{
                    echo "<div id='noResult'>Aucun résultat à votre recherche, soyez plus précis.</div>";
                }

            ?>
        </div>

    </div>

</body>
</html>