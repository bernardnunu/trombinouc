<?php require 'php/auth.php'; ?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/comment.css">
    <script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
    <title>Trombinouc - Commentaire</title>
</head>
<body>

<div id="mainLayout">

    <h1 id="title">Commentaires <span class="fas fa-comments"></span></h1>

    <?php

    if(empty($_GET['id']) || !isset($_GET['id'])){
        header("Location: profil.php");
        exit();
    }

    $com = $bdd->query("SELECT * FROM com
    INNER JOIN users ON users.user_id = com.user_id
    WHERE com.post_id = ".$_GET['id']."
    ORDER BY com.date DESC");
    if($com->rowCount() > 0){
        foreach($com as $comDisplay){
            $date = new DateTime($comDisplay['date']);
            $dateChange = $date->format('d/m/Y à H:i');
            $display = '<div class="comdiv">
                <p class="trombi">Réponse de '.$comDisplay['trombitag'].' :</p>
                <p class="text">'.base64_decode($comDisplay['text']).'</p>
                <p class="date">Le '.$dateChange.'</p>
            </div>';
            echo $display;
        }
    }else{
        header("Location: profil.php");
        exit();
    }

    ?>

</div>

</body>
</html>