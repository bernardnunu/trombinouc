<?php require 'php/publication.php'; ?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/publication.css">
    <script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
    <title><?= $infoUser['trombitag']; ?> - Publication</title>
</head>
<body>

    <div id="mainLayout">
        <div id="content">
            <h1 id="title">Publication</h1>
            <a href="profil.php" id="profil">Mon profil <span class="fas fa-user"></span></a>
            <form enctype="multipart/form-data" id="form" method="post">
                <label for="image" id="label">Image <span class="fas fa-images"></span></label><br>
                <input type="file" name="upfile" id="image"><br>
                <p class="last">(PNG, JPEG, JPG)</p>
                <label for="text" id="label">Commentaire <span class="fas fa-comment-alt"></span></label><br>
                <textarea name="text" id="text" onkeyup="compte()" placeholder="Racontez votre vie..."><?php if(isset($_SESSION['text'])){ echo $_SESSION['text']; } ?></textarea><br>
                <p>(Minimum de 5 caractères.)</p>
                <p class="last"><span id="desc"></span>/200 caractère(s).</p>
                <input type="submit" name="publier" value="Publier" id="submit">
            </form>
            <?php
            
            if(isset($message)){
                echo "<p class='message'>{$message}</p>";
            }

            ?>
        </div>
    </div>

</body>
<script src='js/publication.js'></script>
</html>