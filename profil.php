<?php require 'php/auth.php'; ?>
<?php
require 'class/friend/Display_friend.php'; 
use Friend_System\Display_friend;
?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/profil.css">
    <script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
    <title>Trombinouc - <?= $infoUser['trombitag'] ?></title>
</head>
<body>

    <div id="mainLayout">
        
        <div class="split left">
            <div id="first">
                <h1 id="prenomNom"><?php echo $infoUser['prenom'].' '.$infoUser['nom'].' ';
                if($infoUser['admin'] == 1){
                    echo '<a href="admin.php"><span title="Certifié" class="blue fas fa-user-check"></span></a>';
                } 
                ?></h1>
                <h3 id="trombitag"><?= $infoUser['trombitag'].'#'.$infoUser['user_id']; ?></h3>
                <a href="friend.php" id="friends"><?= Display_friend::gestion($infoUser['user_id'], 1); ?> ami(s) <span class="fas fa-user-friends"></span><?= Display_friend::gestion($infoUser['user_id'], 0); ?></a><br><br><br>
                <a href="modifier-profil.php" class="interact">Modifier le profil <span class="fas fa-user-edit"></span></a><br><br>
                <a href="php/deconnexion.php" class="interact deco" title="Se déconnecter"><span class="fas fa-sign-out-alt"></span></a>
            </div>
            <div id="second">
                <p id="birth"><span class="fas fa-birthday-cake"></span>
                <?php

                if(date('d/m') == substr($infoUser['birth'], 0, -5)){
                    echo "Bon anniversaire !"; 
                }else{
                    echo $infoUser['birth'];
                }

                ?>
                 (<?php 
                
                    $birth = str_replace("/", "-", $infoUser['birth']);
                    $dateNaissance = $birth;
                    $aujourdhui = date("Y-m-d");
                    $diff = date_diff(date_create($dateNaissance), date_create($aujourdhui));
                    $age = $diff->format('%y');
                    echo $age.' ans';

                ?>)
                </p>
                <p id="love">
                <?php
                
                    if($infoUser['love'] == 1){
                        echo "En couple <span class='red fas fa-heart'></span>";
                    }else{
                        echo "Actuellement célibataire <span class='red fas fa-heart-broken'></span>";
                    }

                ?>
                </p>
                <p id="sexe"><?= '<span class="fas fa-venus-mars"></span> '.$infoUser['sexe']; ?></p>
            </div>
            <div id="third">
                <?php 

                    if(isset($infoUser['bio'])){
                        echo '<p id="bio">'.'"'.base64_decode($infoUser['bio']).'"'.'</p>';
                    }else{
                        echo '<p id="bio">...</p>';
                    }

                ?>
                <a href="publication.php" id="post">Nouvelle publication <span class="fas fa-plus-circle"></span></a>
            </div>
        </div>

        <div class="split right">
            <div id="topBar">
                <form action="php/recherche.php" method="GET">
                    <span class="white fas fa-globe"></span>
                    <input type="search" name="barSearch" id="barSearch" placeholder="Chercher quelqu'un...">
                    <input type="submit" name="search" value="Chercher" id="search">
                </form>
            </div>
            <div id="feed">
                <?php

                    $displayPosts = $bdd->prepare("SELECT post.text, post.date, post.dateModif, post.post_id, images.chemin, users.trombitag FROM users
                    INNER JOIN post ON post.user_id = users.user_id 
                    AND users.user_id = ".$infoUser['user_id']."
                    AND post.modify = 0
                    LEFT JOIN postimages ON post.post_id = postimages.post_id
                    LEFT JOIN images ON postimages.image_id = images.image_id AND images.deleted = 0
                    ORDER BY post.date DESC");
                    $displayPosts->execute();
                    $count = $displayPosts->rowCount();
                    if($count > 0){
                        foreach($displayPosts as $post){
                            $datetime1 = new DateTime($post['date']);
                            $datetime2 = new DateTime("now");
                            $interval = $datetime1->diff($datetime2);
                            $array = (array)$interval;
                            if($array['y'] == 0 && $array['m'] == 0 && $array['d'] == 0 && $array['h'] == 0 && $array['i'] == 0){
                                if($array['s'] > 1){
                                    $s = "s";
                                }else{
                                    $s = "";
                                }
                                $date = $interval->format('Il y a %s seconde'.$s.'');
                            }elseif($array['y'] == 0 && $array['m'] == 0 && $array['d'] == 0 && $array['h'] == 0){
                                if($array['i'] > 1){
                                    $s = "s";
                                }else{
                                    $s = "";
                                }
                                $date = $interval->format('Il y a %i minute'.$s.'');
                            }elseif($array['y'] == 0 && $array['m'] == 0 && $array['d'] == 0){
                                if($array['h'] > 1){
                                    $s = "s";
                                }else{
                                    $s = "";
                                }
                                $date = $interval->format('Il y a %h heure'.$s.'');
                            }elseif($array['y'] == 0 && $array['m'] == 0){
                                if($array['d'] > 1){
                                    $s = "s";
                                }else{
                                    $s = "";
                                }
                                $date = $interval->format('Il y a %d jour'.$s.'');
                            }

                            if(isset($post['chemin'])){
                                $img = "<div><a href='".$post['chemin']."' target='__BLANK'><img class='img' alt='Image post de ".$post['trombitag']."' src='".$post['chemin']."'></a></div>";
                            }else{
                                $img = "";
                            }

                            if($post['dateModif']){
                                $modifier = '<p class="modifier">(Modifié)</p>';
                            }else{
                                $modifier = "";
                            }

                            $postDisplay = '<div class="post">
                                <div class="top">
                                <p class="trombitag">Par '.$post['trombitag'].'</p>
                                '.$modifier.'
                                </div>
                                '.$img.'
                                <p class="commentaire">'.base64_decode($post['text']).'</p>
                                <p class="time">'.$date.'</p>
                                <p class="edit"><a title="Modifier" href="post.php?id='.$post['post_id'].'" class="btnModif"><span class="fas fa-edit"></span ></a> <a title="Supprimer" href="php/delete.php?postId='.$post['post_id'].'&userId='.$infoUser['user_id'].'" class="btnDelete"><span class="fas fa-trash-alt"></span></a></p>';
                            $com = $bdd->query("SELECT com.date, com.text, users.trombitag FROM com
                            INNER JOIN users ON users.user_id = com.user_id
                            WHERE com.post_id = ".$post['post_id']."
                            ORDER BY com.date DESC");
                            $countCom = 0;
                            $countSee = 0;
                            if($com->rowCount() > 0){
                                foreach($com as $comDisplay){
                                    if($countCom < 2){
                                        $date = new DateTime($comDisplay['date']);
                                        $dateChange = $date->format('d/m/Y à H:i');
                                        $postDisplay .= '<div class="comdiv">
                                            <p class="com trombi">Réponse de '.$comDisplay['trombitag'].' :</p>
                                            <p class="com text">'.base64_decode($comDisplay['text']).'</p>
                                            <p class="com date">Le '.$dateChange.'</p>
                                        </div>';
                                        $countCom++;
                                    }else{
                                        if($countSee === 0){
                                            $postDisplay .= '<a href="comments.php?id='.$post['post_id'].'" class="next">Afficher tous les commentaires ?</a>';
                                            $countSee++;
                                        }
                                    }
                                }
                            }else{
                                $postDisplay .= "<p class='pasdecom'>Ce post n'a pas de commentaire.</p>";
                            }
                            echo $postDisplay .= '</div>';
                        }
                    }else{
                        echo "<div class='post'>
                            <p class='noPost'>Vous n'avez aucun post sur votre mur.</p>
                            <p class='noPost'>Pour publier un post, rendez-vous sur <span class='newPost'>\"Nouvelle publication\"</span> !</p>
                            </div>";
                    }

                ?>
            </div>
        </div>

    </div>

</body>
</html>