<?php

namespace Friend_System;

/**
 * Friend
 */
class Display_friend{
    
    /**
     * id_1
     *
     * @var mixed
     */
    private $id_1;    
    /**
     * id_2
     *
     * @var mixed
     */
    private $id_2;    
    /**
     * info
     *
     * @var mixed
     */
    private $info;    
    /**
     * bdd
     *
     * @var mixed
     */
    private $bdd;    
    /**
     * friend
     *
     * @var mixed
     */
    public $friend;    
    
    /**
     * __construct
     *
     * @param  mixed $id_1
     * @param  mixed $id_2
     * @return void
     */
    public function __construct(int $id_1, int $id_2) // 2 paramètres à donner quand on instancie l'objet
    {
        $this->id_1 = $id_1;
        $this->id_2 = $id_2;
    }
    
    /**
     * status
     *
     * @return string
     */
    public function status(): string
    {
        global $bdd;
        $check = $bdd->prepare("SELECT status, user_id_1 FROM friend WHERE (user_id_1 = ".$this->id_1." OR user_id_1 = ".$this->id_2.")
        AND (user_id_2 = ".$this->id_2." OR user_id_2 = ".$this->id_1.")
        ORDER BY date DESC
        LIMIT 1"); // Requête pour afficher l'état d'amitié entre 2 utilisateurs
        $check->execute();
        $info = $check->fetch();
        $this->info = $info['status']; // On donne la valeur du status dans l'attribut (variable) "$info" de l'objet
        if($info == true){
            if($info['status'] == 0){ // Demande
                if($info['user_id_1'] == $this->id_1){
                    return "<p class='status attente'>Demande d'ami envoyée.</p><br>";
                }else{
                    return "<p class='status attente'>Cet utilisateur vous a demandé en ami.</p><br>";
                }
            }elseif($info['status'] == 1){ // Accepté
                $this->friend = 1;
                return "<p class='status accepte'>Vous êtes ami avec cette personne.</p><br>
                        <form method='post'>
                        <input type='submit' name='supprimer' class='delete interact' value='Supprimer'><br><br>
                        </form>";
            }elseif($info['status'] == 2){ // Refusé
                if($info['user_id_1'] == $this->id_1){
                    $refus = "<p class='status refuse'>Vous avez rejeté la demande d'ami de cet utilisateur.</p><br>";
                }else{
                    $refus =  "<p class='status refuse'>Cet utilisateur a refusé votre demande d'ami.</p><br>";
                }
                return "$refus
                        <form method='post'>
                        <input type='submit' name='demander' class='ask interact' value='Demander en ami'><br><br>
                        </form>";
            }elseif($info['status'] == 3){ // Supprimé
                return "<form method='post'>
                        <input type='submit' name='demander' class='ask interact' value='Demander en ami'><br><br>
                        </form>";
            }elseif($info['status'] == 4){ // Bloqué
                $this->friend = 4;
                if($info['user_id_1'] == $this->id_1){
                    $bloque = "<p class='status bloque'>Vous avez bloqué cet utilisateur.</p><br>
                            <form method='post'>
                            <input type='submit' name='debloquer' class='delete interact' value='Débloquer cette personne'><br>
                            </form>";
                }else{
                    $bloque = "<p class='status bloque'>Cet utilisateur vous a bloqué.</p>";
                }
                return "$bloque";
            }
        }else{
            return "<form method='post'>
                    <input type='submit' name='demander' class='ask interact' value='Demander en ami'><br><br>
                    </form>";
        }
    }
    
    /**
     * block
     *
     * @return void
     */
    public function block()
    {
        if($this->info != 4){ // Affichage ou non du bouton pour bloquer un utilisateur
            return "<form method='post'>
                    <input type='submit' name='bloquer' class='ask interact' value='Bloquer'><br>
                    </form>";
        }
    }
    
    
    /**
     * gestion
     *
     * @param  mixed $id
     * @param  mixed $nb_status
     * @param  mixed $option
     * @return void
     */
    public static function gestion($id, $nb_status, $option = '1') // Gestion du nombre d'ami, liste d'ami, demande d'ami et notification pour informer l'utilisateur
    {
        global $bdd;
        $list = $bdd->query("SELECT DISTINCT(user_id_1+user_id_2-$id) FROM friend 
        WHERE user_id_1 = $id OR user_id_2 = $id"); // Liste les utilisateurs avec qui $id a eu des interactions peu importe le sens
        $count = 0;
        $notif = 0;
        $list_gestion = [];
        foreach($list as $id_liste){ // Pour chaque retour de la requête précédente on fait plusieurs traitements
            if($nb_status === 2){ // Si le paramètre passé de la méthode est égal à 2
                $status = $bdd->query("SELECT user_id, trombitag, prenom, nom, love, status, user_id_1 FROM friend
                INNER JOIN users ON (friend.user_id_2+friend.user_id_1-$id) = users.user_id
                WHERE ((user_id_1 = $id_liste[0] AND user_id_2 = $id) 
                OR (user_id_1 = $id AND user_id_2 = $id_liste[0]))
                ORDER BY date DESC LIMIT 1"); // Retourne plusieurs colonnes de la table friend et users pour chaque id retourné de $list
                foreach($status as $tab){ // Pour chaque retour de la requête $status (query), on traite les données selon la valeur du paramètre $option
                    if( !empty($tab) && (($option == 0 && $tab['status'] == $option && $tab['user_id_1'] != $id)
                    || ($option == 1 && $tab['status'] == $option ) )) {
                        array_push($list_gestion, $tab);
                    }
                }
            }else{ // Pour toutes les autres valeurs de $nb_status
                $status = $bdd->prepare("SELECT status, user_id_1 FROM friend
                WHERE (user_id_1 = $id_liste[0] AND user_id_2 = $id) 
                OR (user_id_1 = $id AND user_id_2 = $id_liste[0])
                ORDER BY date DESC LIMIT 1"); // Retourne le status et le user_id_1 de la table friend pour chaque id retourné de $list
                $status->execute();
                $know = $status->fetch(); // On récupère les infos sous la forme d'un tableau
            }
            if($nb_status === 1){ // Si $nb_status égal 1 alors on rentre dans cette condition
                if($know['status'] == 1){
                    $count++; // On ajoute 1 à $count
                }
            }elseif($nb_status === 0){ // Si $nb_status égal 0 alors on rentre dans cette condition
                if($know['status'] == 0 && $know['user_id_1'] != $id){ // On vérifie avant d'ajouter 1 à $notif que le user_id_1 n'est pas soi même pour éviter des problèmes d'incohérences
                    $notif++; // On ajoute 1 à $notif
                }
            }
        }
        // Selon le paramètre $nb_status, on retourne quelque chose
        if($nb_status === 1){
            return $count; // Retourne le nombre d'ami qu'une personne a
        }elseif($nb_status === 0 && $notif > 0){
            return "<span id='notif' class='fas fa-envelope'></span>"; // Retourne une petite enveloppe pour savoir si au moins 1 personne vous a fait une demande d'ami
        }elseif($nb_status === 2){
            return $list_gestion; // On retoune sous forme d'un tableau les résultats de $status et $tab
        }
    }

}