<?php

namespace Friend_System;

/**
 * Action_friend
 */
class Action_friend{
    
    /**
     * demande
     *
     * @param  mixed $id1
     * @param  mixed $id2
     * @return void
     */
    public static function demande($id1, $id2)
    {
        global $bdd;
        $demande = $bdd->prepare('INSERT INTO friend (user_id_1, user_id_2, date, status) VALUES (?, ?, NOW(), 0)');
        $demande->execute([$id1, $id2]);
    }
    
    /**
     * supprimer
     *
     * @param  mixed $id1
     * @param  mixed $id2
     * @return void
     */
    public static function supprimer($id1, $id2)
    {
        global $bdd;
        $demande = $bdd->prepare('INSERT INTO friend (user_id_1, user_id_2, date, status) VALUES (?, ?, NOW(), 3)');
        $demande->execute([$id1, $id2]);
    }
    
    /**
     * bloquer
     *
     * @param  mixed $id1
     * @param  mixed $id2
     * @return void
     */
    public static function bloquer($id1, $id2)
    {
        global $bdd;
        $demande = $bdd->prepare('INSERT INTO friend (user_id_1, user_id_2, date, status) VALUES (?, ?, NOW(), 4)');
        $demande->execute([$id1, $id2]);
    }
    
    /**
     * debloquer
     *
     * @param  mixed $id1
     * @param  mixed $id2
     * @return void
     */
    public static function debloquer($id1, $id2)
    {
        global $bdd;
        $demande = $bdd->prepare('DELETE FROM friend WHERE (user_id_1 = '.$id1.' OR user_id_1 = '.$id2.') AND (user_id_2 = '.$id2.' OR user_id_2 = '.$id1.') AND status = 4');
        $demande->execute([$id1, $id2]);
    }
    
    /**
     * accepter
     *
     * @param  mixed $id1
     * @param  mixed $id2
     * @return void
     */
    public static function accepter($id1, $id2)
    {
        global $bdd;
        $demande = $bdd->prepare('INSERT INTO friend (user_id_1, user_id_2, date, status) VALUES (?, ?, NOW(), 1)');
        $demande->execute([$id1, $id2]);
    }
    
    /**
     * refuser
     *
     * @param  mixed $id1
     * @param  mixed $id2
     * @return void
     */
    public static function refuser($id1, $id2)
    {
        global $bdd;
        $demande = $bdd->prepare('INSERT INTO friend (user_id_1, user_id_2, date, status) VALUES (?, ?, NOW(), 2)');
        $demande->execute([$id1, $id2]);
    }

}