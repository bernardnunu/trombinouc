<?php require 'php/user.php'; ?>
<?php 
require 'class/friend/Display_friend.php'; 
use Friend_System\Display_friend;
?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/resultats.css">
    <script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
    <title>Trombinouc- Ami(s) de <?= $currentUser['trombitag'] ?></title>
</head>
<body>
    
    <div id="mainLayout">

        <div id="topBar">
            <h1 id="title">Ami(s) de <?= $currentUser['trombitag'] ?></h1>
            <div id="profil"><a href="profil.php">Mon profil <span class="fas fa-user"></span></a> <a href="user.php?tag=<?= $currentUser['trombitag'] ?>">Son profil</a></div>
        </div>
    
        <div id="result">
            <?php

                $request = Display_friend::gestion($currentUser['user_id'], 2);
                if(!empty($request)){
                    foreach($request as $display){
                        if($display['love'] == 1){
                            $love = "<span class='red fas fa-heart'></span>";
                        }else{
                            $love = "<span class='red fas fa-heart-broken'></span>";
                        }
                        echo "<div class='line'>
                            <a href='user.php?tag=".$display['trombitag']."'>
                            ".$love." ".$display['prenom']." ".$display['nom']."
                            <br>
                            ".$display['trombitag']."#".$display['user_id']."
                            </a>
                        </div>";
                    }
                }else{
                    echo "<div id='noResult'>Cet utilisateur n'a pas d'ami...<br>
                    Soyez le premier à devenir son ami !</div>";
                }

            ?>
        </div>

    </div>

</body>
</html>