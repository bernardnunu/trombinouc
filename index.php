<?php require 'php/check.php'; ?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/index.css">
    <script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
    <title>Trombinouc, le réseau social de demain.</title>
</head>
<body>
    
    <div id="mainLayout">
        <div id="content">
            <h1><span class="fas fa-globe"></span> Trombinouc</h1>
            <a href="inscription.php"><span class="fas fa-keyboard"></span> S'inscrire</a>
            <a href="connexion.php"><span class="fas fa-sign-in-alt"></span> Connexion</a>
        </div>
    </div>

</body>
</html>