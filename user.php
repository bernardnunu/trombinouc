<?php require 'php/user.php'; ?>
<?php 
require 'class/friend/Display_friend.php'; 
use Friend_System\Display_friend;
?>
<?php require 'class/friend/Action_friend.php'; ?>
<?php require 'php/action.php'; ?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/profil.css">
    <script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
    <title>Trombinouc - <?= $currentUser['trombitag'] ?></title>
</head>
<body>
    <div id="mainLayout">
        <div class="split left">
            <div id="first">
                <a href="profil.php" class="interact">Mon profil <span class="fas fa-user"></span></a><br><br>
                <h1 id="prenomNom"><?php echo $currentUser['prenom'].' '.$currentUser['nom'].' ';
                if($currentUser['admin'] == 1){
                    echo '<span title="Certifié" class="blue fas fa-user-check"></span>';
                } 
                ?></h1>
                <h3 id="trombitag"><?= $currentUser['trombitag'].'#'.$currentUser['user_id']; ?></h3>
                <a href="friend-list.php?tag=<?= $currentUser['trombitag'] ?>" id="friends"><?= Display_friend::gestion($currentUser['user_id'], 1); ?> ami(s) <span class="fas fa-user-friends"></span></a><br><br>
                <?= $display_friend->status(); ?>
                <?= $display_friend->block(); ?>
            </div>
            <div id="second">
                <p id="birth"><span class="fas fa-birthday-cake"></span>
                <?php 
                
                if(date('d/m') == substr($currentUser['birth'], 0, -5)){
                    echo "C'est l'anniversaire de ".$currentUser['trombitag']." !"; 
                }else{
                    echo $currentUser['birth'];
                } 
                
                ?>
                 (<?php 
                
                    $birth = str_replace("/", "-", $currentUser['birth']);
                    $dateNaissance = $birth;
                    $aujourdhui = date("Y-m-d");
                    $diff = date_diff(date_create($dateNaissance), date_create($aujourdhui));
                    $age = $diff->format('%y');
                    echo $age.' ans';

                ?>)
                </p>
                <p id="love">
                <?php
                
                    if($currentUser['love'] == 1){
                        echo "En couple <span class='red fas fa-heart'></span>";
                    }else{
                        echo "Actuellement célibataire <span class='red fas fa-heart-broken'></span>";
                    }

                ?>
                </p>
                <p id="sexe"><?= '<span class="fas fa-venus-mars"></span> '.$currentUser['sexe']; ?></p>
            </div>
            <div id="third">
                <?php 

                    if(isset($currentUser['bio'])){
                        echo '<p id="bio">'.'"'.base64_decode($currentUser['bio']).'"'.'</p>';
                    }else{
                        echo '<p id="bio">...</p>';
                    }

                ?>
            </div>
        </div>

        <div class="split right">
            <div id="topBar">
                <form action="php/recherche.php" method="GET">
                    <span class="white fas fa-globe"></span>
                    <input type="search" name="barSearch" id="barSearch" placeholder="Chercher quelqu'un...">
                    <input type="submit" name="search" value="Chercher" id="search">
                </form>
            </div>
            <div id="feed">
                <?php

                    $displayPosts = $bdd->prepare("SELECT post.post_id, post.text, post.date, post.dateModif, images.chemin, users.trombitag FROM users
                    INNER JOIN post ON post.user_id = users.user_id 
                    AND users.user_id = ".$currentUser['user_id']."
                    AND post.modify = 0
                    LEFT JOIN postimages ON post.post_id = postimages.post_id
                    LEFT JOIN images ON postimages.image_id = images.image_id AND images.deleted = 0
                    ORDER BY post.date DESC");
                    $displayPosts->execute();
                    $count = $displayPosts->rowCount();
                    if($display_friend->friend != 4){
                        if($count > 0){
                            foreach($displayPosts as $post){
                                $datetime1 = new DateTime($post['date']);
                                $datetime2 = new DateTime("now");
                                $interval = $datetime1->diff($datetime2);
                                $array = (array)$interval;
                                if($array['y'] == 0 && $array['m'] == 0 && $array['d'] == 0 && $array['h'] == 0 && $array['i'] == 0){
                                    if($array['s'] > 1){
                                        $s = "s";
                                    }else{
                                        $s = "";
                                    }
                                    $date = $interval->format('Il y a %s seconde'.$s.'');
                                }elseif($array['y'] == 0 && $array['m'] == 0 && $array['d'] == 0 && $array['h'] == 0){
                                    if($array['i'] > 1){
                                        $s = "s";
                                    }else{
                                        $s = "";
                                    }
                                    $date = $interval->format('Il y a %i minute'.$s.'');
                                }elseif($array['y'] == 0 && $array['m'] == 0 && $array['d'] == 0){
                                    if($array['h'] > 1){
                                        $s = "s";
                                    }else{
                                        $s = "";
                                    }
                                    $date = $interval->format('Il y a %h heure'.$s.'');
                                }elseif($array['y'] == 0 && $array['m'] == 0){
                                    if($array['d'] > 1){
                                        $s = "s";
                                    }else{
                                        $s = "";
                                    }
                                    $date = $interval->format('Il y a %d jour'.$s.'');
                                }

                                if(isset($post['chemin'])){
                                    $img = "<div><a href='".$post['chemin']."' target='__BLANK'><img class='img' alt='Image post de ".$post['trombitag']."' src='".$post['chemin']."'></a></div>";
                                }else{
                                    $img = "";
                                }

                                if($display_friend->friend === 1){
                                    $var = '<form class="edit btnModif" method="post">
                                            <input type="checkbox" id="check" style="display: none">
                                            <label style="cursor: pointer" for="check"><span class="fas fa-comment"></span></label>
                                            <input type="hidden" name="postid" value='.$post['post_id'].'>
                                            <textarea name="text_reply" placeholder="Répondre à '.$currentUser['trombitag'].'" id="comment"></textarea>
                                            <input id="reply" type="submit" name="reply" value="Répondre">
                                        </form>';
                                }else{
                                    $var = '';
                                }

                                if(isset($message)){
                                    $alert = '<p class="alert">'.$message.'</p>';
                                }else{
                                    $alert = '';
                                }

                                $postDisplay = '<div class="post">
                                    <div class="top">
                                    <p class="trombitag">Par '.$post['trombitag'].'</p>
                                    </div>
                                    '.$img.'
                                    <p class="commentaire">'.base64_decode($post['text']).'</p>
                                    <p class="time">'.$date.'</p>
                                    '.$var.'
                                    '.$alert.'';
                                $com = $bdd->query("SELECT * FROM com
                                INNER JOIN users ON users.user_id = com.user_id
                                WHERE com.post_id = ".$post['post_id']."
                                ORDER BY com.date DESC");
                                $countCom = 0;
                                $countSee = 0;
                                if($com->rowCount() > 0){
                                    foreach($com as $comDisplay){
                                        if($countCom < 2){
                                            $date = new DateTime($comDisplay['date']);
                                            $dateChange = $date->format('d/m/Y à H:i');
                                            $postDisplay .= '<div class="comdiv">
                                                <p class="com trombi">Réponse de '.$comDisplay['trombitag'].' :</p>
                                                <p class="com text">'.base64_decode($comDisplay['text']).'</p>
                                                <p class="com date">Le '.$dateChange.'</p>
                                            </div>';
                                            $countCom++;
                                        }else{
                                            if($countSee === 0){
                                                $postDisplay .= '<a href="comments.php?id='.$post['post_id'].'" class="next">Afficher tous les commentaires ?</a>';
                                                $countSee++;
                                            }
                                        }
                                    }
                                }else{
                                    $postDisplay .= "<p class='pasdecom'>Ce post n'a pas de commentaire.</p>";
                                }
                                echo $postDisplay .= '</div>';
                            }
                        }else{
                            echo "<div class='post'>
                                <p class='noPost'>Cet utilisateur n'a aucune publication sur son mur.</p>
                                </div>";
                        }
                    }else{
                        echo "<div class='post'>
                            <p class='noPost'>Vous ne pouvez pas voir les posts de cet utilisateur.</p>
                            </div>";
                    }

                ?>
            </div>
        </div>

    </div>

</body>
</html>