<?php

use Friend_System\Action_friend;

if(isset($_POST['accepter'])){ // Si on clique sur le input submit avec le name "accepter" on appelle la méthode static accepter
    Action_friend::accepter($infoUser['user_id'], $_POST['userid']);
}elseif(isset($_POST['refuser'])){ // Si on clique sur le input submit avec le name "refuser" on appelle la méthode static refuser
    Action_friend::refuser($infoUser['user_id'], $_POST['userid']);
}