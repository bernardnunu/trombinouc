<?php

require 'bdd.php';

// On vérifie que l'utilisateur est cliqué sur "chercher"
if(isset($_GET['search'])){
    // On vérifie que le contenu de la recherche n'est pas vide
    if(!empty($_GET['barSearch'])){
        // On vérifie la forme du contenu de la recherche avec une ER
        if(!preg_match("/[;*<>\[\]\|&\/\$]/", $_GET['barSearch'])){
            // On vérifie la taille en octet du contenu de la recherche
            if(iconv_strlen($_GET['barSearch']) >= 2 && iconv_strlen($_GET['barSearch']) <= 20){
                // On redirige l'utilisateur vers la page de résultat(s) de sa recherche 
                header("Location: /resultats.php?search=".trim($_GET['barSearch'])."");
                exit();

            }else{
                header("Location: /profil.php");
                exit();
            }
        }else{
            header("Location: /profil.php");
            exit();
        }
    }else{
        header("Location: /profil.php");
        exit();
    }
}