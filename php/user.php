<?php

require 'php/auth.php';

// On vérifie si le paramètre tag existe dans l'URL et si il n'est pas vide
if(!isset($_GET['tag']) || empty(trim($_GET['tag'])) || trim($_GET['tag']) === $infoUser['trombitag']){
    header("Location: /profil.php");
    exit();
}

// On définit une variable avec comme contenu la string du paramètre de l'URL
$tag = trim($_GET['tag']);
// On vérifie si le TrombiTag existe dans la base de donnée
$exist = $bdd->prepare("SELECT * FROM users WHERE trombitag = ?");
$exist->execute(array($tag));
$currentUser = $exist->fetch(); // On récupère les infos de la requête avec un fetch

// Si le TrombiTag n'existe pas, on redirige l'utilisateur sur son profil
if($currentUser == false){
    header("Location: /profil.php");
    exit();
}