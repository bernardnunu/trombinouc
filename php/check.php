<?php

session_start();

// On vérifie si l'utlisateur n'a pas déjà un cookie de la bonne forme pour le rediriger

if(isset($_COOKIE['user']) && !empty($_COOKIE['user'])){
    header("Location: /profil.php");
    exit();
}