<?php

require 'bdd.php';

// On vérifie que le cookie existe et qu'il n'est pas vide

if(isset($_COOKIE['user']) && !empty($_COOKIE['user'])){

    // On récupère l'IP de l'utilisateur avec une fonction qui gère les proxy

    function getIp(){
        return hash("sha256", $_SERVER['REMOTE_ADDR']);
    }

    $ip = getIp();

    // On décortique le cookie de l'utilisateur pour faire des vérifications sur sa validité

    $cookieValue = explode("-", $_COOKIE['user']);
    $searchCookie = $bdd->prepare("SELECT * FROM users WHERE user_id = ?");
    $searchCookie->execute(array($cookieValue[0]));
    $infoUser = $searchCookie->fetch();

    // On vérifie si le unique_id du cookie correpond avec le unique_id dans la base de donnée

    if($cookieValue[1] === $infoUser['unique_id']){
        // On vérifie si l'IP du cookie correpond avec l'IP dans la base de donnée
        if($cookieValue[2] === $infoUser['user_ip'] && $cookieValue[2] === $ip){
          // On définit une variable pour prévenir que l'utilisateur est valide
          $valide = true;
        }else{
            setcookie('user', '', '');
            header("Location: /");
            exit();
        }
    }else{
        setcookie('user', '', '');
        header("Location: /");
        exit();
    }

}else{
    header("Location: /");
    exit();
}