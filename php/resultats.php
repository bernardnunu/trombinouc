<?php 

require 'php/auth.php';

// On vérifie qu'il y ait bien un paramètre search dans l'URL et qu'elle n'est pas vide
if(!isset($_GET['search']) || empty(trim($_GET['search']))){
  // Si l'URL n'est pas de la bonne forme, l'utilisateur est rediriger vers son profil
  header("Location: /profil.php");
  exit();
}