<?php

require 'bdd.php';

// Déconnexion de l'utilisateur en supprimant le cookie, en updatant les données dans la base de donnée et en redirigeant l'utilisateur

if(isset($_COOKIE['user'])){
    $userID = explode("-", $_COOKIE['user']);
    setcookie('user', '', '', '/');
    $null = $bdd->prepare("UPDATE users SET user_ip = NULL WHERE user_id = ?");
    $null->execute(array($userID[0]));
    header("Location: /");
    exit();
}else{
    header("Location: /");
    exit();
}