<?php

session_start();

require 'php/auth.php';

// Traitement à la publication

function fill($nom){ // Fonction pour conserver les données rentrées par l'uilisateur au cas ou il se trompe dans le formulaire
    if(isset($_POST[$nom])){
        $_SESSION[$nom] = $_POST[$nom];
    }
}

if(isset($_POST['publier'])){ // J'ai déjà commenté la page php/post.php (même chose)
    if($valide == true){
        fill('text');
        if(!empty($_POST['text'])){
            if(!preg_match("/[;*<>\[\]\|&\/\$]/", $_POST['text'])){
                if(iconv_strlen(str_replace("\n", "", $_POST['text'])) >= 5 && iconv_strlen(str_replace("\n", "", $_POST['text'])) <= 200){
                    if(substr_count(nl2br($_POST['text']), "<br />") <= 4){

                        if($_FILES['upfile']['size'] != 0){
                            
                            $extensions = array("jpeg","jpg","png");
                            $fileExtention = pathinfo($_FILES['upfile']['name'], PATHINFO_EXTENSION);

                            if(in_array($fileExtention, $extensions) === true){
                            
                                if($_FILES['upfile']['size'] < 10000000){

                                    $typeMime = array("image/jpeg", "image/png");
                                    $upload_dir = 'upload/images/'.uniqid().'.'.$fileExtention;
                                    $tmp_name = $_FILES['upfile']['tmp_name'];

                                    if(in_array(mime_content_type($tmp_name), $typeMime)){

                                        try{

                                            $image = new Imagick($tmp_name);
                                            $orientation = $image->getImageOrientation();

                                            switch($orientation) {
                                                case 2:
                                                    $image->flopImqge();
                                                break;
                                        
                                                case 3:
                                                    $image->rotateimage("#000", 180);
                                                break;
                
                                                case 4:
                                                    $image->rotateimage("#000", 180);
                                                    $image->flopImqge();
                                                break;
                                        
                                                case 5:
                                                    $image->rotateimage("#000", 90);
                                                    $image->flopImqge();
                                                break;
                
                                                case 6:
                                                    $image->rotateimage("#000", 90);
                                                break;
                
                                                case 7:
                                                    $image->rotateimage("#000", -90);
                                                    $image->flopImqge();
                                                break;
                
                                                case 8:
                                                    $image->rotateimage("#000", -90);
                                                break;
                                            }
                                            $resize = $image->thumbnailImage(380, 0);
                                            $upload = $image->writeImage($upload_dir);
                
                                            if($resize){
                                                if($upload){
                
                                                    try{
                                                        $bdd->beginTransaction();
                                                        $bddPost = $bdd->prepare("INSERT INTO post (text, date, user_id) VALUES (?, NOW(), ?)");
                                                        $bddImages = $bdd->prepare("INSERT INTO images (chemin, dateAjout) VALUES (?, NOW())");
                                                        $bddPostImages = $bdd->prepare("INSERT INTO postimages (post_id, image_id, date) VALUES (?, ?, NOW())");
                                                        $bddPost->execute(array(base64_encode(nl2br($_POST['text'])), $infoUser['user_id']));
                                                        $lastIdPost = $bdd->lastInsertId();
                                                        $bddImages->execute(array($upload_dir));
                                                        $lastIdImage = $bdd->lastInsertId();
                                                        $bddPostImages->execute(array($lastIdPost, $lastIdImage));
                                                        if($bdd->commit()){
                                                            header("Location: /profil.php");
                                                            exit();
                                                        }
                                                    }catch(Exception $e){
                                                        $bdd->rollBack();
                                                        $message = "Failed: " . $e->getMessage();
                                                    }

                
                                                }else{
                                                    $message = "Erreur lors de l'écriture de l'image.";
                                                }

                                            }else{
                                                $message = "Erreur lors du redimensionnement de l'image.";
                                            }

                                        }catch(Exception $e) {
                                            $message = "Erreur lors de la mise en ligne de votre post.";
                                        }

                                    }else{
                                        $message = "Le fichier n'est pas une image.";
                                    }

                                }else{
                                    $message = "Votre image ne doit pas dépasser 10 MB.";
                                }

                            }else{  
                                $message = "L'extention de votre image n'est pas correcte.";
                            }

                        }else{

                            $new = $bdd->prepare("INSERT INTO post (text, date, user_id) VALUES (?, NOW(), ?)");
                            if($new->execute(array(base64_encode(nl2br($_POST['text'])), $infoUser['user_id']))){
                                header("Location: /profil.php");
                                exit();
                            }else{
                                $message = "Une erreur est survenue lors de la publication.";
                            }

                        }
                    
                    }else{
                        $message = "4 sauts de ligne maximum autorisés.";
                    }
                }else{
                    $message = "Votre commentaire doit être compris entre 5 et 200 caractères.";
                }
            }else{
                $message = "Commentaire non conforme.";
            }
        }else{
            $message = "Veuillez renseigner un commentaire.";
        }
    }else{
        header("deconnexion.php");
        exit();
    }
}