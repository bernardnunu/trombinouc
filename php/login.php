<?php

require 'bdd.php';

function fill($nom){ // Fonction pour conserver les données rentrées par l'uilisateur au cas ou il se trompe dans le formulaire
    if(isset($_POST[$nom])){
        $_SESSION[$nom] = $_POST[$nom];
    }
}

// Traitement des champs pour la connexion

if(isset($_POST['connexion'])){
    fill('email');
    if(!empty($_POST['email'] && !empty($_POST['mdp']))){ // On vérifie que les champs ne sont pas vides
        // On vérifie que l'email rentré par l'utilisateur soit de la bonne forme avec une expression régulière.
        if(preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", $_POST['email'])){
            // On vérifie que le mot de passe rentré par l'utilisateur soit de la bonne forme avec une expression régulière.
            if(preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{5,20}$/", $_POST['mdp'])){

                // On vérifie si l'email existe dans la base de donnée

                $emailExist = $bdd->prepare("SELECT * FROM users WHERE email = ?");
                $emailExist->execute(array($_POST['email']));
                $infoEmail = $emailExist->fetch();

                if($infoEmail == true){

                    // On vérifie si l'email et le mot de passe match pour connecter l'utilisateur

                    $mdpExist = $bdd->prepare("SELECT * FROM users WHERE email = ? AND password = ?");
                    $mdpExist->execute(array($_POST['email'], hash("sha256", $_POST['mdp'])));
                    $info = $mdpExist->fetch();

                    if($info == true){

                        // On récupère l'IP de l'utilisateur et on le hash

                        function getIp(){
                            return hash("sha256", $_SERVER['REMOTE_ADDR']);
                        }

                        $ip = getIp();

                        // On entre la donnée de l'IP hashé dans la base de donnée

                        $updateIp = $bdd->prepare("UPDATE users SET user_ip = ? WHERE user_id = ".$info['user_id']."");
                        if(! $updateIp->execute(array($ip))){
                            $message = "Une erreur s'est produite lors de la connexion.";
                        }else{
                            // On crée la string qui sera le contenu du cookie du user
                            $string = $info['user_id'].'-'.$info['unique_id'].'-'.$ip;
                            // On crée le cookie d'une durée de 1 semaine
                            setcookie('user', $string, time()+86400*7, '/', '', false, true);
                            // On redirige l'utilisateur sur sa page de profil
                            header("Location: /profil.php");
                            exit();
                        }

                    }else{
                        $message = "Le mot de passe est incorrect.";
                    }

                }else{
                    $message = "Aucun compte ne possède cet email.";
                }

            }else{
                $message = "Veuillez saisir un mot de passe conforme.";
            }
        }else{
            $message = "Veuillez saisir un email conforme.";
        }
    }else{
        $message = "Veuillez renseigner tous les champs.";
    }
}