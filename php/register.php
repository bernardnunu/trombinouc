<?php

require 'bdd.php';

function fill($nom){ // Fonction pour conserver les données rentrées par l'uilisateur au cas ou il se trompe dans le formulaire
    if(isset($_POST[$nom])){
        $_SESSION[$nom] = $_POST[$nom];
    }
}

if(isset($_POST['register'])){
    fill('prenom'); // appel de la fonction fill sur le champs prenom
    fill('nom'); // appel de la fonction fill sur le champs nom
    fill('trombitag'); // appel de la fonction fill sur le champs trombitag
    fill('email'); // appel de la fonction fill sur le champs email
    fill('birth'); // appel de la fonction fill sur le champs birth
    fill('bio'); // appel de la fonction fill sur le champs bio
    if(!empty($_POST['prenom'] && !empty($_POST['nom']) && !empty($_POST['trombitag']) && !empty($_POST['email']) && !empty($_POST['mdp']) && !empty($_POST['mdp1']) && !empty($_POST['birth']))){
    // On vérifie que les champs ne sont pas vide
        if(isset($_POST['love'])){ // On définit la variable love selon le choix de l'utilisateur
            $love = 1;
        }else{
            $love = 0;
        }

        // On vérifie que tous les champs rentrés par l'utilisateur sont de la forme attendue avec des regex
        if(preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", $_POST['email'])){
            if(preg_match("/^[A-Z][A-Za-z\é\è\ê\-]{2,24}+$/", $_POST['prenom'])){
                if(preg_match("/^[A-Z][A-Za-z\é\è\ê\-]{2,24}+$/", $_POST['nom'])){
                    if(preg_match("/^([0][1-9]|[1-2][0-9]|30|31)\/([0][1-9]|10|11|12)\/(19[0-9][0-9]|20[0-1][0-9]|2020)/", $_POST['birth'])){
                        if($_POST['sexe'] == "Femme" || $_POST['sexe'] == "Homme"){ // On vérifie que l'utilisateur n'a pas changé les valeurs de sexe
                            if(!preg_match("/[;*<&>\[\]\|&\/\$]/", $_POST['bio'])){
                                if(iconv_strlen($_POST['bio']) <= 250){ // On vérifie la taille de la bio rentrée
                                    if(preg_match("/^[a-zA-Z0-9_\-]{3,20}$/", $_POST['trombitag'])){
                                        if(preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{5,20}$/", $_POST['mdp'])){
                                            if($_POST['mdp'] === $_POST['mdp1']){ // On vérifie que les 2 mdp correspondent

                                                $birth = str_replace("/", "-", $_POST['birth']); // On remplace les / par des - pour la date de naissance
                                                $dateNaissance = $birth;
                                                $aujourdhui = date("Y-m-d"); // Date du jour de l'inscription
                                                $diff = date_diff(date_create($dateNaissance), date_create($aujourdhui)); // On fait la différence entre la date de naissance et la date du jour
                                                $age = $diff->format('%y'); // On prend que le résultat en années

                                                if($age >= 18){ // On vérifie que l'utilisateur a 18 ans

                                                // On vérifie que l'email rentré n'existe pas déjà
                                                $verifyEmail = $bdd->prepare("SELECT * FROM users WHERE email = ?");
                                                $verifyEmail->execute(array($_POST['email']));
                                                $infoEmail = $verifyEmail->fetch();

                                                if($infoEmail == false){
                                                    
                                                    // On vérifie que le trombitag rentré n'existe pas déjà
                                                    $verifyTrombi = $bdd->prepare("SELECT * FROM users WHERE trombitag = ?");
                                                    $verifyTrombi->execute(array($_POST['trombitag']));
                                                    $infoTrombi = $verifyTrombi->fetch();

                                                    if($infoTrombi == false){

                                                        // On vérifie si la bio existe ou pas et on définit la variable $bio
                                                        if(!empty($_POST['bio'])){
                                                            $bio64 = base64_encode($_POST['bio']);
                                                        }else{
                                                            $bio64 = NULL;
                                                        }

                                                        $hashPassword = hash("sha256", $_POST['mdp']); // On sécurise le mdp en le cryptant en sha256
                                                        $unique_id = uniqid("", true); // On génére un identifiant unique

                                                        // On rentre toutes les infos de l'utilisateur dans la base de donnée pour l'inscrire
                                                        $inscription = $bdd->prepare("INSERT INTO users (prenom, nom, trombitag, email, password, birth, bio, love, sexe, unique_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                                                        // On vérifie que l'inscription s'est bien passée
                                                        if($inscription->execute(array($_POST['prenom'], $_POST['nom'], $_POST['trombitag'], $_POST['email'], $hashPassword, $_POST['birth'], $bio64, $love, $_POST['sexe'], $unique_id))){
                                                            // On redirige l'utilisateur sur la page de connexion
                                                            header("Location: connexion.php");
                                                            exit();

                                                        }else{
                                                            $message = "Une erreur s'est produite lors de l'inscription.";
                                                        }

                                                    }else{
                                                        $message = "Ce TrombiTag est déjà utilisé.";
                                                    }

                                                }else{
                                                    $message = "Cet email est déjà utilisé.";
                                                }

                                                }else{
                                                    $message = "Vous devez avoir au moins 18 ans pour inscrire.";
                                                }

                                            }else{
                                                $message = "Vos mots de passe ne correspondent pas.";
                                            }   
                                        }else{
                                            $message = "Veuillez saisir un mot de passe sécurisé.";
                                        }
                                    }else{
                                        $message = "Veuillez saisir un TrombiTag conforme.";
                                    }
                                }else{
                                    $message = "La biographie ne doit pas dépasser 250 caractères.";
                                }
                            }else{
                                $message = "Caractère(s) non autorisé(s) dans la biographie.";
                            }
                        }else{
                            $message = "Veuillez saisir un sexe conforme.";
                        }
                    }else{
                        $message = "Veuillez saisir une date de naissance conforme.";
                    }
                }else{
                    $message = "Veuillez saisir un nom conforme.";
                }
            }else{
                $message = "Veuillez saisir un prénom conforme.";
            }
        }else{
            $message = "Veuillez saisir un Email valide.";
        }

    }else{
        $message = "Veuillez renseigner tous les champs.";
    }
}