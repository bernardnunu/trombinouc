<?php

require 'php/auth.php';

// On vérifie que l'utilisateur est valide

if(isset($_POST['modifier']) && $valide == true){
    // On vérifie que les champs ne soient pas vides
    if(!empty($_POST['prenom'] && !empty($_POST['nom']) && !empty($_POST['trombitag']) && !empty($_POST['email']) && !empty($_POST['mdp1']) && !empty($_POST['birth']))){
        
    // On définit la variable love qui nous servira comme entrée dans la base de donnée

    if(isset($_POST['love'])){
        $love = 1;
    }else{
        $love = 0;
    }

    if(empty(trim($_POST['mdp']))){
        $mdp = $_POST['mdp1'];
    }else{
        $mdp = $_POST['mdp'];
    }

    if(preg_match("/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", $_POST['email'])){ // On vérifie la forme de l'email avec une ER
        if(preg_match("/^[A-Z][A-Za-z\é\è\ê\-]{2,24}+$/", $_POST['prenom'])){ // On vérifie la forme du prénom avec une ER
            if(preg_match("/^[A-Z][A-Za-z\é\è\ê\-]{2,24}+$/", $_POST['nom'])){ // On vérifie la forme du nom avec une ER
                if(preg_match("/^([0][1-9]|[1-2][0-9]|30|31)\/([0][1-9]|10|11|12)\/(19[0-9][0-9]|20[0-1][0-9]|2020)/", $_POST['birth'])){ // On vérifie la forme de la date avec une ER
                    if($_POST['sexe'] == "Femme" || $_POST['sexe'] == "Homme"){ // On vérifie que les valeurs sont soit "Homme" soit "Femme"
                        if(!preg_match("/[;*<&>\[\]\|&\/\$]/", $_POST['bio'])){ // On vérifie la forme de la bio avec une ER
                            if(iconv_strlen($_POST['bio']) <= 250){ // On vérifie que la bio soit inférieur à 250 octets
                                if(preg_match("/^[a-zA-Z0-9_\-]{3,20}$/", $_POST['trombitag'])){ // On vérifie la forme du TrombiTag avec une ER
                                    if(preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{5,20}$/", $mdp)){ // On vérifie la forme du mot de passe avec une ER
                                        if(preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{5,20}$/", $_POST['mdp1'])){ // On vérifie la forme du mot de passe avec une ER

                                        // On vérifie si le mot de passe actuel est correct
                                        $oldMdp = $bdd->prepare("SELECT * FROM users WHERE password = ? AND user_id = ".$infoUser['user_id']."");
                                        $oldMdp->execute(array(hash("sha256", $_POST['mdp1'])));
                                        $goodMdp = $oldMdp->fetch();
                                        if($goodMdp == true){
                                            
                                            // On vérifie que l'utilisateur soit bien majeur
                                            $birth = str_replace("/", "-", $_POST['birth']);
                                            $dateNaissance = $birth;
                                            $aujourdhui = date("Y-m-d");
                                            $diff = date_diff(date_create($dateNaissance), date_create($aujourdhui));
                                            $age = $diff->format('%y');

                                            if($age >= 18){
                                            // On vérifie que l'email n'est pas déjà pris
                                            $verifyEmail = $bdd->prepare("SELECT * FROM users WHERE email = ? AND user_id != ".$infoUser['user_id']."");
                                            $verifyEmail->execute(array($_POST['email']));
                                            $infoEmail = $verifyEmail->fetch();
                                            if($infoEmail == false){
                                                // On vérifie que le TrombiTag n'est pas déjà pris
                                                $verifyTrombi = $bdd->prepare("SELECT * FROM users WHERE trombitag = ? AND user_id != ".$infoUser['user_id']."");
                                                $verifyTrombi->execute(array($_POST['trombitag']));
                                                $infoTrombi = $verifyTrombi->fetch();
                                                if($infoTrombi == false){
                                                    // On vérifie si la bio est vide ou non
                                                    if(!empty($_POST['bio'])){
                                                        $bio64 = base64_encode($_POST['bio']);
                                                    }else{
                                                        $bio64 = NULL;
                                                    }
                                                    // On sécurise le mot de passe dans la base de donnée en le hashant
                                                    $hashPassword = hash("sha256", $mdp);
                                                    $unique_id = uniqid("", true);
                                                    // On update la table user pour modifier le profil de l'utilisateur
                                                    $modifierProfil = $bdd->prepare("UPDATE users SET prenom = ?, nom = ?, trombitag = ?, email = ?, password = ?, birth = ?, bio = ?, love = ?, sexe = ? WHERE user_id = ".$infoUser['user_id']."");
                                                    if($modifierProfil->execute(array($_POST['prenom'], $_POST['nom'], $_POST['trombitag'], $_POST['email'], $hashPassword, $_POST['birth'], $bio64, $love, $_POST['sexe']))){
                                                        // On redirige l'utilisateur sur son profil si tout est bon
                                                        header("Location: profil.php");
                                                        exit();

                                                    }else{
                                                        $message = "Une erreur s'est produite lors de l'inscription.";
                                                    }

                                                }else{
                                                    $message = "Ce TrombiTag est déjà utilisé.";
                                                }

                                            }else{
                                                $message = "Cet email est déjà utilisé.";
                                            }

                                            }else{
                                                $message = "Vous devez avoir au moins 18 ans sur le site.";
                                            }
                                        }else{
                                        $message = "Votre mot de passe actuel est incorrect.";
                                        }
                                        }else{
                                        $message = "Actuel mot de passe non conforme.";
                                        }
                                    }else{
                                        $message = "Veuillez saisir un nouveau mot de passe sécurisé.";
                                    }
                                }else{
                                    $message = "Veuillez saisir un TrombiTag conforme.";
                                }
                            }else{
                                $message = "La biographie ne doit pas dépasser 250 caractères.";
                            }
                        }else{
                            $message = "Caractère(s) non autorisé(s) dans la biographie.";
                        }
                    }else{
                        $message = "Veuillez saisir un sexe conforme.";
                    }
                }else{
                    $message = "Veuillez saisir une date de naissance conforme.";
                }
            }else{
                $message = "Veuillez saisir un nom conforme.";
            }
        }else{
            $message = "Veuillez saisir un prénom conforme.";
        }
    }else{
        $message = "Veuillez saisir un Email valide.";
    }

}else{
    $message = "Veuillez renseigner tous les champs.";
}
}