<?php

use Friend_System\{
    Display_friend,
    Action_friend
}; 

$display_friend = new Display_friend($infoUser['user_id'], $currentUser['user_id']);

if(isset($_POST['demander'])){
    Action_friend::demande($infoUser['user_id'], $currentUser['user_id']);
}elseif(isset($_POST['supprimer'])){
    Action_friend::supprimer($infoUser['user_id'], $currentUser['user_id']);
}elseif(isset($_POST['bloquer'])){
    Action_friend::bloquer($infoUser['user_id'], $currentUser['user_id']);
}elseif(isset($_POST['debloquer'])){
    Action_friend::debloquer($infoUser['user_id'], $currentUser['user_id']);
}elseif(isset($_POST['accepter'])){
    Action_friend::accepter($infoUser['user_id'], $_POST['userid']);
}elseif(isset($_POST['refuser'])){
    Action_friend::refuser($infoUser['user_id'], $_POST['userid']);
}elseif(isset($_POST['reply'])){
    if(!empty($_POST['text_reply'])){
        if(!preg_match("/[;*<>\[\]\|&\/\$]/", $_POST['text_reply'])){
            if(iconv_strlen($_POST['text_reply']) < 150 && iconv_strlen($_POST['text_reply']) > 10){

                $already = $bdd->prepare("SELECT * FROM com WHERE user_id = ? AND post_id = ?");
                $already->execute([$infoUser['user_id'], $_POST['postid']]);
                $exist = $already->fetch();

                if($exist == false){

                    $insert = $bdd->prepare("INSERT INTO com (text, date, user_id, post_id) VALUES (?, NOW(), ?, ?)");
                    if($insert->execute(array(base64_encode($_POST['text_reply']), $infoUser['user_id'], $_POST['postid']))){
                        header("Location: ".$_SERVER['REQUEST_URI']."");
                        exit();
                    }

                }else{
                    $message = "Vous avez déjà répondu à ce post (max 1).";
                }

            }else{
                $message = "Votre commentaire doit être compris entre 10 et 150 caractères.";
            }

        }
    }else{
        $message = "Veuillez renseigner un commentaire.";
    }
}