<?php

require 'auth.php';

$check = $bdd->prepare("SELECT * FROM post II
LEFT JOIN postimages I ON I.post_id = II.post_id 
WHERE II.post_id = ".$_GET['id']." AND II.user_id = ".$infoUser['user_id']."");
$check->execute(); // On vérifie que l'id du post corresponde à l'id de l'utilisateur
$know = $check->fetch();

// Check si c'est le bon utilisateur à l'arrivée.

if($know == false){ // Si jamais les deux id ne correpondent pas, on déconnecte l'utilisateur
    header("Location: php/deconnexion.php");
    exit();
}else{
    $ok = true;
}

function fill($nom){ // Fonction pour conserver les données rentrées par l'uilisateur au cas ou il se trompe dans le formulaire
    if(isset($_POST[$nom])){
        $_SESSION[$nom] = $_POST[$nom];
    }
}

// Traitement de modification

if(isset($_POST['modifier'])){
    fill('text');
    if($ok == true){ // On vérifie que la variable $ok est bien à true
        if(!empty($_POST['text'])){ // On vérifie que le champs text n'est pas vide
            if(!preg_match("/[;*<>\[\]\|&\/\$]/", $_POST['text'])){ // On vérifie que le champs text est de la forme attendue
                if(iconv_strlen(str_replace("\n", "", $_POST['text'])) >= 5 && iconv_strlen(str_replace("\n", "", $_POST['text'])) <= 200){ // On vérifie la longueur de text corresponde
                    if(substr_count(nl2br($_POST['text']), "<br />") <= 4){

                        if($_FILES['upfile']['size'] != 0){ // On vérifie si l'utilisateur veut mettre une image ou non
    
                            $extensions = array("jpeg","jpg","png"); // Tableau avec les extensions autorisées
                            $fileExtention = pathinfo($_FILES['upfile']['name'], PATHINFO_EXTENSION); // Extension du fichier donné

                            if(in_array($fileExtention, $extensions) === true){ // Si l'extension du fichier est bon, on continue
                            
                                if($_FILES['upfile']['size'] < 10000000){ // On vérifie que l'image est inférieure à 10 MB

                                    $typeMime = array("image/jpeg", "image/png"); // Tableau avec les types MIME autorisés
                                    $upload_dir = 'upload/images/'.uniqid().'.'.$fileExtention; // Variable pour aller dans le dossier ou l'on va importer l'image
                                    $tmp_name = $_FILES['upfile']['tmp_name']; // Chemin temporaire de l'image avant l'upload

                                    if(in_array(mime_content_type($tmp_name), $typeMime)){ // On vérifie que l'image soit du bon type MIME

                                        try{

                                            $image = new Imagick($tmp_name); // On instancie l'objet Imagik pour gérer l'image de l'utilisateur
                                            $orientation = $image->getImageOrientation(); // On regarde l'orientation de l'image pour la repositionner au cas ou elle n'est pas dans le bon sens

                                            switch($orientation) { // Plusieurs cas possible dans l'orientation
                                                case 2: // Retournée horizontalement
                                                    $image->flopImqge(); // Mirroir sur l'axe Y
                                                break;
                                        
                                                case 3: // Image inversée (-180 degrés)
                                                    $image->rotateimage("#000", 180);
                                                break;
                
                                                case 4: // Image inversée et retournée horizontalement
                                                    $image->rotateimage("#000", 180);
                                                    $image->flopImqge(); // Mirroir sur l'axe Y
                                                break;
                                        
                                                case 5: // Image en -90 degrés et retournée horizontalement 
                                                    $image->rotateimage("#000", 90);
                                                    $image->flopImqge(); // Mirroir sur l'axe Y
                                                break;
                
                                                case 6: // Image en -90 degrés
                                                    $image->rotateimage("#000", 90);
                                                break;
                
                                                case 7: // Image en -90 degrés et retournée horizontalement
                                                    $image->rotateimage("#000", -90);
                                                    $image->flopImqge(); // Mirroir sur l'axe Y
                                                break;
                
                                                case 8: // Image en -90 degrés
                                                    $image->rotateimage("#000", -90);
                                                break;
                                            }
                                            $resize = $image->thumbnailImage(380, 0); // Redemension de l'image 380px de largeur
                                            $upload = $image->writeImage($upload_dir); // On importe l'image traitée dans la chemin correspondant
                
                                            if($resize){ // Si pas d'erreur
                                                if($upload){ // Si pas d'erreur
                                                    
                                                    if($know['postimages_id']){ // Si le post avait déjà une image attachée avant la modification

                                                        try{ // On capture les exceptions avec un try/catch
                                                            $bdd->beginTransaction(); // On démarre une transaction (désactive le mode autocommit)
                                                            // On fait des updates, des insertions et des suppressions dans la base de donnée
                                                            $bddGetImage = $bdd->prepare("SELECT * FROM postimages II INNER JOIN images I ON II.image_id = I.image_id WHERE post_id = ? AND I.deleted = false ORDER BY I.dateAjout DESC LIMIT 1;");
                                                            $bddModifyImage = $bdd->prepare("UPDATE images SET DeletedDate = NOW(), deleted = ? WHERE image_id = ?");
                                                            $bddDeleteImage = $bdd->prepare("DELETE FROM postimages WHERE post_id = ? AND image_id = ?");
                                                            $bddImage = $bdd->prepare("INSERT INTO images (chemin, dateAjout) VALUES (?, NOW());");
                                                            $update = $bdd->prepare("UPDATE post SET modify = 1, dateModif = NOW()
                                                            WHERE post_id = ".$_GET['id']." AND user_id = ".$infoUser['user_id']."");
                                                            $date = $bdd->prepare("SELECT date FROM post WHERE post_id = ".$_GET['id']."");
                                                            $insert = $bdd->prepare("INSERT INTO post (text, date, user_id, dateModif) VALUES (?, ?, ?, NOW())");
                                                            $updateCom = $bdd->prepare("UPDATE com SET post_id = ? WHERE post_id = ?");
                                                            $postimages = $bdd->prepare("INSERT INTO postimages (post_id, image_id, date) VALUES (?, ?, NOW());");
                                                            $bddGetImage->execute(array($_GET['id']));
                                                            $requeteImage = $bddGetImage->fetch();
                                                            $idImage = intval($requeteImage['image_id']);
                                                            $bddModifyImage->execute(array(1, $idImage));
                                                            $bddDeleteImage->execute(array($_GET['id'], $idImage));
                                                            $bddImage->execute(array($upload_dir));
                                                            $lastIdImage = $bdd->lastInsertId();
                                                            $update->execute();
                                                            $date->execute();
                                                            $infoDate = $date->fetch();
                                                            $insert->execute(array(base64_encode(nl2br($_POST['text'])), $infoDate['date'], $infoUser['user_id']));
                                                            $lastIdPost = $bdd->lastInsertId();
                                                            $updateCom->execute(array($lastIdPost, $_GET['id']));
                                                            $postimages->execute(array($lastIdPost, $lastIdImage));
                                                            if($bdd->commit()){ // Valide la transaction et remet la connexion en mode autocommit
                                                                header("Location: /profil.php");
                                                                exit();
                                                            }
                                                        }
                                                        catch(Exception $e){
                                                            $bdd->rollBack(); // Annule la transaction courante, initiée par beginTransaction()
                                                            $message = "Failed: " . $e->getMessage();
                                                        }

                                                    }else{
                                                        
                                                        try{
                                                            $bdd->beginTransaction(); // On démarre une transaction (désactive le mode autocommit)
                                                            // On fait des updates, des insertions et des suppressions dans la base de donnée
                                                            $bddImage = $bdd->prepare("INSERT INTO images (chemin, dateAjout) VALUES (?, NOW());");
                                                            $ID = $bdd->prepare("SELECT date FROM post WHERE post_id = ".$_GET['id']."");
                                                            $update = $bdd->prepare("UPDATE post SET modify = 1, dateModif = NOW()
                                                            WHERE post_id = ".$_GET['id']." AND user_id = ".$infoUser['user_id']."");
                                                            $insert = $bdd->prepare("INSERT INTO post (text, date, user_id, dateModif) VALUES (?, ?, ?, NOW())");
                                                            $updateCom = $bdd->prepare("UPDATE com SET post_id = ? WHERE post_id = ?");
                                                            $postimages = $bdd->prepare("INSERT INTO postimages (post_id, image_id, date) VALUES (?, ?, NOW());");
                                                            $bddImage->execute(array($upload_dir));
                                                            $lastIdImage = $bdd->lastInsertId();
                                                            $ID->execute();
                                                            $lastId = $ID->fetch();
                                                            $update->execute();
                                                            $insert->execute(array(base64_encode(nl2br($_POST['text'])), $lastId['date'], $infoUser['user_id']));
                                                            $lastIdPost = $bdd->lastInsertId();
                                                            $updateCom->execute(array($lastIdPost, $_GET['id']));
                                                            $postimages->execute(array($lastIdPost, $lastIdImage));
                                                            if($bdd->commit()){ // Valide la transaction et remet la connexion en mode autocommit
                                                                header("Location: /profil.php");
                                                                exit();
                                                            }
                                                        }
                                                        catch(Exception $e){
                                                            $bdd->rollBack(); // Annule la transaction courante, initiée par beginTransaction()
                                                            $message = "Failed: " . $e->getMessage();
                                                        }

                                                    }
                
                                                }else{
                                                    $message = "Erreur lors de l'écriture de l'image.";
                                                }

                                            }else{
                                                $message = "Erreur lors du redimensionnement de l'image.";
                                            }

                                        }catch(Exception $e) {
                                            $message = "Erreur lors de la mise en ligne de votre post.";
                                        }

                                    }else{
                                        $message = "Le fichier n'est pas une image.";
                                    }

                                }else{
                                    $message = "Votre image ne doit pas dépasser 10 MB.";
                                }

                            }else{  
                                $message = "L'extention de votre image n'est pas correcte.";
                            }


                        }else{

                            try{
                                $bdd->beginTransaction(); // On démarre une transaction (désactive le mode autocommit)
                                // On fait des updates, des insertions et des suppressions dans la base de donnée
                                $ID = $bdd->prepare("SELECT date FROM post WHERE post_id = ".$_GET['id']."");
                                $update = $bdd->prepare("UPDATE post SET modify = 1, dateModif = NOW()
                                WHERE post_id = ".$_GET['id']." AND user_id = ".$infoUser['user_id']."");
                                $insert = $bdd->prepare("INSERT INTO post (text, date, user_id, dateModif) VALUES (?, ?, ?, NOW())");
                                $updateCom = $bdd->prepare("UPDATE com SET post_id = ? WHERE post_id = ?");
                                $ID->execute();
                                $lastId = $ID->fetch();
                                $update->execute();
                                $insert->execute(array(base64_encode(nl2br($_POST['text'])), $lastId['date'], $infoUser['user_id']));
                                $lastIdPost = $bdd->lastInsertId();
                                $updateCom->execute(array($lastIdPost, $_GET['id']));
                                if($bdd->commit()){ // Valide la transaction et remet la connexion en mode autocommit
                                    header("Location: /profil.php");
                                    exit();
                                }
                            }
                            catch(Exception $e){
                                $bdd->rollBack(); // Annule la transaction courante, initiée par beginTransaction()
                                $message = "Failed: " . $e->getMessage();
                            }

                        }

                    }else{
                        $message = "4 sauts de ligne maximum autorisés.";
                    }
                }else{
                    $message = "Votre commentaire doit être compris entre 5 et 200 caractères.";
                }
            }else{
                $message = "Commentaire non conforme";
            }
        }else{
            $message = "Veuillez renseigner un commentaire.";
        }
    }else{
        header("Location: deconnexion.php");
        exit();
    }
}