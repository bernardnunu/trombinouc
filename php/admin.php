<?php

require 'bdd.php';

require 'php/auth.php';

$admin = $bdd->prepare("SELECT admin FROM users WHERE user_id = ?");
$admin->execute(array($cookieValue[0]));
$okornot = $admin->fetch();

if($okornot['admin'] != 1){
    header("Location: php/deconnexion.php");
    exit();
}