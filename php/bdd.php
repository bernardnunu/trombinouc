<?php

// Connexion à la base de donnée avec l'objet PDO en capturant les exceptions

try{
    $bdd = new PDO('mysql:host=127.0.0.1;dbname=trombinouc;charset=utf8', 'trombinouc', 'qwerty-123$');
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e){
    echo "Erreur : " . $e->getMessage();
}