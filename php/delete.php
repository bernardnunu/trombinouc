<?php

require 'auth.php';

// On récupère les paramètres passés dans l'URL

$post_id = $_GET['postId'];
$user_id = $_GET['userId'];

// On vérifie qu'il y ait bien une entrée dans la base de donnée qui corresponde aux paramètres

$check = $bdd->prepare("SELECT * FROM post WHERE post_id = $post_id AND user_id = $user_id");
$check->execute();
$info = $check->fetch();

// Si ça match on update la table post pour supprimer le post tout en gardant l'historique

if($info == true){
    $delete = $bdd->prepare("UPDATE post SET modify = 1, dateDelete = NOW() WHERE post_id = $post_id AND user_id = $user_id");
    if($delete->execute()){
        header("Location: /profil.php");
        exit();
    }

// Si ça match pas, on déconnecte l'utilisateur

}else{
    header("Location: deconnexion.php");
    exit();
}
