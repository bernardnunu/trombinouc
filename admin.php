<?php require 'php/admin.php' ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Listes des utilisateurs</title>
</head>
<body>
    
    <?php

        if(isset($_POST['suppr'])){
            try
            {
                $bdd->beginTransaction();
                $delete = $bdd->prepare("DELETE FROM users WHERE user_id = ?");
                $delete->execute(array($_POST['userid']));
                if($bdd->commit()){
                    echo "Utilisateur supprimé";
                }
            }
            catch(Exception $e){
                $bdd->rollBack();
                echo $message = "Failed: " . $e->getMessage();
            }
        }

        $req = $bdd->query("SELECT * FROM users");

        foreach($req as $display){

            echo '<div style="text-align: center; margin-bottom: 20px; border: solid 1px black;">
                '.$display['prenom'].'<br>
                '.$display['nom'].'<br>
                '.$display['trombitag'].'<br>
                '.$display['email'].'<br>
                <a href="user.php?tag='.$display['trombitag'].'">Profil</a>
                <form method="post">
                    <input type="hidden" name="userid" value="'.$display['user_id'].'">
                    <input type="submit" name="suppr" value="Supprimer">
                </form>
            </div>';

        }

    ?>

</body>
</html>