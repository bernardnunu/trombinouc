<?php require 'php/check.php'; ?>
<?php require 'php/login.php'; ?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/form.css">
    <title>Trombinouc - Connexion</title>
</head>
<body>

    <div id="mainLayout">
        <div id="content">
            <h1 id="title">Connexion</h1>
            <form method="post" id="form">
                <input type="email" name="email" class="input" placeholder="Email" value="<?php if(isset($_SESSION['email'])){ echo $_SESSION['email'];} ?>">
                <input type="password" name="mdp" class="input" placeholder="Mot de passe"><br>
                <input type="submit" name="connexion" value="Se connecter" class="submit">
            </form>
            <?php
            
            if(isset($message)){
                echo "<p class='message'>{$message}</p>";
            }

            ?>
            <a href="https://www.instagram.com/matthieumrll/?hl=fr" target="_BLANK" class="option">Problème(s) pour vous connecter ?</a><br>
            <a href="inscription.php" class="option">Vous n'avez pas de compte ?</a>
        </div>
    </div>

</body>
</html>