<?php require 'php/auth.php'; ?>
<?php 
require 'class/friend/Display_friend.php'; 
use Friend_System\Display_friend;
?>
<?php require 'class/friend/Action_friend.php'; ?>
<?php require 'php/demand.php'; ?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/resultats.css">
    <script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
    <title>Trombinouc- Amis</title>
</head>
<body>
    
    <div id="mainLayout">

        <div id="topBar">
            <h1 id="title">Demande(s) d'ami(s)</h1>
            <div id="profil"><a class href="profil.php">Mon profil <span class="fas fa-user"></span></a> <a href="friend.php">Mes amis</a><br> <br><a href="demand.php">Demande(s) d'ami(s)<?= Display_friend::gestion($infoUser['user_id'], 0); ?></a></div>
        </div>
    
        <div id="result">
            <?php

                $request = Display_friend::gestion($infoUser['user_id'], 2, 0);
                if(!empty($request)){
                    foreach($request as $display){
                        if($display['love'] == 1){
                            $love = "<span class='red fas fa-heart'></span>";
                        }else{
                            $love = "<span class='red fas fa-heart-broken'></span>";
                        }
                        echo "<div class='line demand'><a href='user.php?tag=".$display['trombitag']."'>".$love." ".$display['prenom']." ".$display['nom']." ".$display['trombitag']."#".$display['user_id']."
                        <form class='form' method='post'>
                            <input type='hidden' name='userid' value='".$display['user_id_1']."'>
                            <input class='option accepter' type='submit' name='accepter' value='Accepter'>
                        </form>
                        <form method='post'>
                            <input type='hidden' name='userid' value='".$display['user_id_1']."'>
                            <input class='option refuser' type='submit' name='refuser' value='Refuser'>
                        </form>
                        </a></div>";
                    }
                }else{
                    echo "<div id='noResult'>Vous n'avez pas de demande d'ami...<br>
                    Ne soyez pas déprimé, ce jour arrivera !</div>";
                }

            ?>
        </div>

    </div>

</body>
</html>