<?php require 'php/modifier-profil.php'; ?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/form.css">
    <script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
    <title>Trombinouc - Modifier profil</title>
</head>
<body>

    <div id="mainLayout">
        <div id="content">
            <h1 id="title">Modifier</h1>
            <a href="profil.php" class="interact">Mon profil <span class="fas fa-user"></span></a><br><br>
            <p class="info plus">/!\ Champs obligatoires (*)</p>
            <form method="post" id="form">
                <input id="textPrenom" type="text" name="prenom" class="input" onkeyup="compte1()" placeholder="Prénom (*)" value="<?php echo $infoUser['prenom']; ?>">
                <p class="info"><span id="prenom"></span>/25 caractère(s).</p>
                <input id="textNom" type="text" name="nom" class="input" onkeyup="compte2()" placeholder="Nom (*)" value="<?php echo $infoUser['nom']; ?>">
                <p class="info"><span id="nom"></span>/25 caractère(s).</p>
                <select name="sexe" class="sexe">
                    <option>Homme</option>
                    <option <?php if($infoUser['sexe'] === "Femme"){ echo "selected"; } ?>>Femme</option>
                </select><br>
                <input id="textTrombitag" type="text" name="trombitag" class="input" onkeyup="compte3()" placeholder="TrombiTag (*)" value="<?php echo $infoUser['trombitag']; ?>">
                <p class="info">Le "TrombiTag" est un pseudonyme.<br><span id="trombitag"></span>/20 caractère(s).</p>
                <input type="text" name="email" class="input1" placeholder="Email (*)" value="<?php echo $infoUser['email']; ?>">
                <input type="text" name="birth" pattern="^([0][1-9]|[1-2][0-9]|30|31)\/([0][1-9]|10|11|12)\/(19[0-9][0-9]|20[0-1][0-9]|2020)" class="input1" id="field-birthday" placeholder="Date de naissance (*)" value="<?php echo $infoUser['birth']; ?>">
                <input type="password" name="mdp" class="input1" placeholder="Nouveau mot de passe">
                <input type="password" name="mdp1" class="input1" placeholder="Mot de passe actuel (*)">
                <textarea id="textBio" style="width: 80%" type="text" name="bio" class="input" onkeyup="compte()" placeholder="Biographie"><?php echo base64_decode($infoUser['bio']); ?></textarea>
                <p class="info"><span id="bio"></span>/250 caractère(s).</p>
                <label for="love" class="label">En couple ?</label>
                <input type="checkbox" name="love" class="love" <?php if($infoUser['love'] == 1){ echo "checked"; } ?>><br>
                <input type="submit" name="modifier" value="Modifier" class="submit">
            </form>
            <?php
            
            if(isset($message)){
                echo "<p class='message'>{$message}</p>";
            }

            ?>
        </div>
    </div>

</body>
<script src='js/inscription.js'></script>
</html>