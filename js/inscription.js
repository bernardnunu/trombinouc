// Fonction slashs automatiques pour la date de naissance

function T() {
    document.getElementById("field-birthday").value = document.getElementById("field-birthday").value.replace(/^(\d{2})$/g, "$1/"), 
    document.getElementById("field-birthday").value = document.getElementById("field-birthday").value.replace(/^(\d{2})\/(\d{2})$/g, "$1/$2/"), 
    document.getElementById("field-birthday").value = document.getElementById("field-birthday").value.replace(/\/\//g, "/")
}

document.getElementById("field-birthday").addEventListener("keyup", function() 
    {
        var e=event.keyCode || event.charCode;
        return 8!==e&&46!==e&&(T(),!1)
    }
)

// Fonction compteur de caractère biographie

function compte(){var txt=document.getElementById('textBio').value;
    document.getElementById('bio').innerHTML= txt.replace(/\[\/?\w+\]/g,'').length;
}

// Fonction compteur de caractère prenom

function compte1(){var txt=document.getElementById('textPrenom').value;
    document.getElementById('prenom').innerHTML= txt.replace(/\[\/?\w+\]/g,'').length;
}

// Fonction compteur de caractère nom

function compte2(){var txt=document.getElementById('textNom').value;
    document.getElementById('nom').innerHTML= txt.replace(/\[\/?\w+\]/g,'').length;
}

// Fonction compteur de caractère trombitag

function compte3(){var txt=document.getElementById('textTrombitag').value;
    document.getElementById('trombitag').innerHTML= txt.replace(/\[\/?\w+\]/g,'').length;
}