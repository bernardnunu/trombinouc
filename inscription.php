<?php require 'php/check.php'; ?>
<?php require 'php/register.php'; ?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/form.css">
    <title>Trombinouc - Inscription</title>
</head>
<body>

    <div id="mainLayout">
        <div id="content">
            <h1 id="title">Inscription</h1>
            <p class="info plus">/!\ Champs obligatoires (*)</p>
            <form method="post" id="form">
                <input id="textPrenom" type="text" name="prenom" class="input" onkeyup="compte1()" placeholder="Prénom (*)" value="<?php if(isset($_SESSION['prenom'])){echo $_SESSION['prenom'];} ?>">
                <p class="info"><span id="prenom"></span>/25 caractère(s).</p>
                <input id="textNom" type="text" name="nom" class="input" onkeyup="compte2()" placeholder="Nom (*)" value="<?php if(isset($_SESSION['nom'])){echo $_SESSION['nom'];} ?>">
                <p class="info"><span id="nom"></span>/25 caractère(s).</p>
                <select name="sexe" class="sexe">
                    <option>Homme</option>
                    <option>Femme</option>
                </select><br>
                <input id="textTrombitag" type="text" name="trombitag" class="input" onkeyup="compte3()" placeholder="TrombiTag (*)" value="<?php if(isset($_SESSION['trombitag'])){echo $_SESSION['trombitag'];} ?>">
                <p class="info">Le "TrombiTag" est un pseudonyme.<br><span id="trombitag"></span>/20 caractère(s).</p>
                <input type="text" name="email" class="input1" placeholder="Email (*)" value="<?php if(isset($_SESSION['email'])){echo $_SESSION['email'];} ?>">
                <input type="text" name="birth" pattern="^([0][1-9]|[1-2][0-9]|30|31)\/([0][1-9]|10|11|12)\/(19[0-9][0-9]|20[0-1][0-9]|2020)" class="input1" id="field-birthday" placeholder="Date de naissance (*)" value="<?php if(isset($_SESSION['birth'])){echo $_SESSION['birth'];} ?>">
                <input type="password" name="mdp" class="input1" placeholder="Mot de passe (*)">
                <input type="password" name="mdp1" class="input1" placeholder="Confirmer mot de passe (*)">
                <textarea id="textBio" style="width: 80%" type="text" name="bio" class="input" onkeyup="compte()" placeholder="Biographie"><?php if(isset($_SESSION['bio'])){echo $_SESSION['bio'];} ?></textarea>
                <p class="info"><span id="bio"></span>/250 caractère(s).</p>
                <label for="love" class="label">En couple ?</label>
                <input type="checkbox" name="love" class="love"><br>
                <input type="submit" name="register" value="S'inscrire" class="submit">
            </form>
            <?php
            
            if(isset($message)){
                echo "<p class='message'>{$message}</p>";
            }

            ?>
            <a href="connexion.php" class="option">Vous avez déjà un compte ?</a>
        </div>
    </div>

</body>
<script src='js/inscription.js'></script>
</html>